package com.tora;

import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    private static final String FILENAME = "bigdecimals.txt";
    private static final int NO_OF_ELEMENTS = 1000;

    public static void main(String[] args) throws IOException {
        generateBigDecimalFile();
        sumAndAvgOfBigDecimalFile();
        topTenPercentOfBigDecimalFile();
    }

    public static void writeBigDecimalToStream(BigDecimal bigDecimal, ObjectOutputStream outputStream) throws IOException {
        var unscaledValueByteArray = bigDecimal.unscaledValue().toByteArray();
        outputStream.writeInt(unscaledValueByteArray.length);
        outputStream.write(unscaledValueByteArray);
        outputStream.writeInt(bigDecimal.scale());
    }

    public static BigDecimal readBigDecimalFromStream(ObjectInputStream objectInputStream) throws IOException {
        var unscaledLength = objectInputStream.readInt();
        var unscaled = objectInputStream.readNBytes(unscaledLength);
        var scale = objectInputStream.readInt();

        return new BigDecimal(new BigInteger(unscaled), scale);
    }

    public static BigDecimal generateRandomBigDecimal() {
        return new BigDecimal(BigInteger.valueOf(new Random().nextInt(1000000000)), 2);
    }

    public static void generateBigDecimalFile() throws IOException {
        try (
                var objectOutputStream = new ObjectOutputStream(new FileOutputStream(FILENAME))
        ) {
            for (int i = 0; i < NO_OF_ELEMENTS; i++) {
                writeBigDecimalToStream(generateRandomBigDecimal(), objectOutputStream);
            }
        }
    }

    public static void sumAndAvgOfBigDecimalFile() throws IOException {
        try (
                var objectInputStream = new ObjectInputStream(new FileInputStream(FILENAME))
        ) {
            var sum = IntStream.range(0, NO_OF_ELEMENTS)
                    .mapToObj(i -> {
                        try {
                            return readBigDecimalFromStream(objectInputStream);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            System.out.println("SUM: " + sum);
            System.out.println("AVG: " + sum.divide(new BigDecimal(NO_OF_ELEMENTS), RoundingMode.CEILING));
        }
    }

    public static void topTenPercentOfBigDecimalFile() throws IOException {
        try (
                var objectInputStream = new ObjectInputStream(new FileInputStream(FILENAME))
        ) {
            var top10 = IntStream.range(0, NO_OF_ELEMENTS)
                    .mapToObj(i -> {
                        try {
                            return readBigDecimalFromStream(objectInputStream);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .sorted(Comparator.reverseOrder())
                    .limit((long) (NO_OF_ELEMENTS * 0.1))
                    .collect(Collectors.toList());

            System.out.println(top10);
        }
    }
}