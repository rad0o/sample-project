package com.tora;

import com.tora.entity.Order;
import com.tora.repo.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import com.tora.CommonStates.*;

import java.util.concurrent.TimeUnit;

@Fork(value = 3)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 20, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 20, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class AddBenchmarks {
    @Benchmark
    public void ArrayListAddBenchmark(Blackhole blackhole, ArrayListState arrayListState) {
        arrayListState.repo.add(CommonStates.OrdersState.order1);
        arrayListState.repo.add(OrdersState.order2);
        arrayListState.repo.add(OrdersState.order3);
        arrayListState.repo.add(OrdersState.order4);
        arrayListState.repo.add(OrdersState.order5);
        arrayListState.repo.add(OrdersState.order6);
        arrayListState.repo.add(OrdersState.order7);
        arrayListState.repo.add(OrdersState.order8);
        arrayListState.repo.add(OrdersState.order9);
        arrayListState.repo.add(OrdersState.order10);
        blackhole.consume(arrayListState.repo);
    };

    @Benchmark
    public void HashSetBenchmark(Blackhole blackhole, HashSetState hashSetState) {
        hashSetState.repo.add(OrdersState.order1);
        hashSetState.repo.add(OrdersState.order2);
        hashSetState.repo.add(OrdersState.order3);
        hashSetState.repo.add(OrdersState.order4);
        hashSetState.repo.add(OrdersState.order5);
        hashSetState.repo.add(OrdersState.order6);
        hashSetState.repo.add(OrdersState.order7);
        hashSetState.repo.add(OrdersState.order8);
        hashSetState.repo.add(OrdersState.order9);
        hashSetState.repo.add(OrdersState.order10);
        blackhole.consume(hashSetState.repo);
    };

    @Benchmark
    public void TreeSetBenchmark(Blackhole blackhole, TreeSetState treeSetState) {
        treeSetState.repo.add(OrdersState.order1);
        treeSetState.repo.add(OrdersState.order2);
        treeSetState.repo.add(OrdersState.order3);
        treeSetState.repo.add(OrdersState.order4);
        treeSetState.repo.add(OrdersState.order5);
        treeSetState.repo.add(OrdersState.order6);
        treeSetState.repo.add(OrdersState.order7);
        treeSetState.repo.add(OrdersState.order8);
        treeSetState.repo.add(OrdersState.order9);
        treeSetState.repo.add(OrdersState.order10);
        blackhole.consume(treeSetState.repo);
    };

    @Benchmark
    public void ConcurrentHashMapState(Blackhole blackhole, ConcurrentHashMapState concurrentHashMapState) {
        concurrentHashMapState.repo.add(OrdersState.order1);
        concurrentHashMapState.repo.add(OrdersState.order2);
        concurrentHashMapState.repo.add(OrdersState.order3);
        concurrentHashMapState.repo.add(OrdersState.order4);
        concurrentHashMapState.repo.add(OrdersState.order5);
        concurrentHashMapState.repo.add(OrdersState.order6);
        concurrentHashMapState.repo.add(OrdersState.order7);
        concurrentHashMapState.repo.add(OrdersState.order8);
        concurrentHashMapState.repo.add(OrdersState.order9);
        concurrentHashMapState.repo.add(OrdersState.order10);
        blackhole.consume(concurrentHashMapState.repo);
    };

    @Benchmark
    public void MutableListBenchmark(Blackhole blackhole, MutableListState mutableListState) {
        mutableListState.repo.add(OrdersState.order1);
        mutableListState.repo.add(OrdersState.order2);
        mutableListState.repo.add(OrdersState.order3);
        mutableListState.repo.add(OrdersState.order4);
        mutableListState.repo.add(OrdersState.order5);
        mutableListState.repo.add(OrdersState.order6);
        mutableListState.repo.add(OrdersState.order7);
        mutableListState.repo.add(OrdersState.order8);
        mutableListState.repo.add(OrdersState.order9);
        mutableListState.repo.add(OrdersState.order10);
        blackhole.consume(mutableListState.repo);
    };
    @Benchmark
    public void THashSetBenchmark(Blackhole blackhole, THashSetState tHashSetState) {
        tHashSetState.repo.add(OrdersState.order1);
        tHashSetState.repo.add(OrdersState.order2);
        tHashSetState.repo.add(OrdersState.order3);
        tHashSetState.repo.add(OrdersState.order4);
        tHashSetState.repo.add(OrdersState.order5);
        tHashSetState.repo.add(OrdersState.order6);
        tHashSetState.repo.add(OrdersState.order7);
        tHashSetState.repo.add(OrdersState.order8);
        tHashSetState.repo.add(OrdersState.order9);
        tHashSetState.repo.add(OrdersState.order10);
        blackhole.consume(tHashSetState.repo);
    };
}
