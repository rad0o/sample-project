package com.tora;

import com.tora.entity.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import com.tora.CommonStates.*;

import java.util.concurrent.TimeUnit;

@Fork(value = 3)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 20, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 20, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class RemoveBenchmarks {
    @Benchmark
    public void ArrayListRemoveBenchmark(Blackhole blackhole, ArrayListFullState arrayListFullState) {
        arrayListFullState.repo.remove(new Order(1, 10, 100));
        arrayListFullState.repo.remove(new Order(9, 10, 100));
        arrayListFullState.repo.remove(new Order(2, 10, 100));
        arrayListFullState.repo.remove(new Order(4, 10, 100));
        arrayListFullState.repo.remove(new Order(3, 10, 100));
        arrayListFullState.repo.remove(new Order(7, 10, 100));
        arrayListFullState.repo.remove(new Order(6, 10, 100));
        arrayListFullState.repo.remove(new Order(8, 10, 100));
        arrayListFullState.repo.remove(new Order(10, 10, 100));
        arrayListFullState.repo.remove(new Order(5, 10, 100));
        blackhole.consume(arrayListFullState.repo);
    }
    @Benchmark
    public void ConcurrentHashMapRemoveBenchmark(Blackhole blackhole, ConcurrentHashMapState concurrentHashMapState) {
        concurrentHashMapState.repo.remove(new Order(1, 10, 100));
        concurrentHashMapState.repo.remove(new Order(9, 10, 100));
        concurrentHashMapState.repo.remove(new Order(2, 10, 100));
        concurrentHashMapState.repo.remove(new Order(4, 10, 100));
        concurrentHashMapState.repo.remove(new Order(3, 10, 100));
        concurrentHashMapState.repo.remove(new Order(7, 10, 100));
        concurrentHashMapState.repo.remove(new Order(6, 10, 100));
        concurrentHashMapState.repo.remove(new Order(8, 10, 100));
        concurrentHashMapState.repo.remove(new Order(10, 10, 100));
        concurrentHashMapState.repo.remove(new Order(5, 10, 100));
        blackhole.consume(concurrentHashMapState.repo);
    }
    @Benchmark
    public void HashSetFullStateRemoveBenchmark(Blackhole blackhole, HashSetFullState state) {
        state.repo.remove(new Order(1, 10, 100));
        state.repo.remove(new Order(9, 10, 100));
        state.repo.remove(new Order(2, 10, 100));
        state.repo.remove(new Order(4, 10, 100));
        state.repo.remove(new Order(3, 10, 100));
        state.repo.remove(new Order(7, 10, 100));
        state.repo.remove(new Order(6, 10, 100));
        state.repo.remove(new Order(8, 10, 100));
        state.repo.remove(new Order(10, 10, 100));
        state.repo.remove(new Order(5, 10, 100));
        blackhole.consume(state.repo);
    }
    @Benchmark
    public void MutableListRemoveBenchmark(Blackhole blackhole, MutableListFullState state) {
        state.repo.remove(new Order(1, 10, 100));
        state.repo.remove(new Order(9, 10, 100));
        state.repo.remove(new Order(2, 10, 100));
        state.repo.remove(new Order(4, 10, 100));
        state.repo.remove(new Order(3, 10, 100));
        state.repo.remove(new Order(7, 10, 100));
        state.repo.remove(new Order(6, 10, 100));
        state.repo.remove(new Order(8, 10, 100));
        state.repo.remove(new Order(10, 10, 100));
        state.repo.remove(new Order(5, 10, 100));
        blackhole.consume(state.repo);
    }
    @Benchmark
    public void THashSetRemoveBenchmark(Blackhole blackhole, THashSetFullState state) {
        state.repo.remove(new Order(1, 10, 100));
        state.repo.remove(new Order(9, 10, 100));
        state.repo.remove(new Order(2, 10, 100));
        state.repo.remove(new Order(4, 10, 100));
        state.repo.remove(new Order(3, 10, 100));
        state.repo.remove(new Order(7, 10, 100));
        state.repo.remove(new Order(6, 10, 100));
        state.repo.remove(new Order(8, 10, 100));
        state.repo.remove(new Order(10, 10, 100));
        state.repo.remove(new Order(5, 10, 100));
        blackhole.consume(state.repo);
    }
    @Benchmark
    public void TreeSetRemoveBenchmark(Blackhole blackhole, TreeSetFullState state) {
        state.repo.remove(new Order(1, 10, 100));
        state.repo.remove(new Order(9, 10, 100));
        state.repo.remove(new Order(2, 10, 100));
        state.repo.remove(new Order(4, 10, 100));
        state.repo.remove(new Order(3, 10, 100));
        state.repo.remove(new Order(7, 10, 100));
        state.repo.remove(new Order(6, 10, 100));
        state.repo.remove(new Order(8, 10, 100));
        state.repo.remove(new Order(10, 10, 100));
        state.repo.remove(new Order(5, 10, 100));
        blackhole.consume(state.repo);
    }

}
