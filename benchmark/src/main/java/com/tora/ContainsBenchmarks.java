package com.tora;

import com.tora.entity.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import com.tora.CommonStates.*;

import java.util.concurrent.TimeUnit;

@Fork(value = 3)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 5, time = 20, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 20, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class ContainsBenchmarks {
    @Benchmark
    public void ArrayListContainsBenchmark(Blackhole blackhole, ArrayListFullState state) {
        state.repo.contains(new Order(1, 10, 100));
        state.repo.contains(new Order(9, 10, 100));
        state.repo.contains(new Order(2, 10, 100));
        state.repo.contains(new Order(8, 10, 100));
        state.repo.contains(new Order(3, 10, 100));
        state.repo.contains(new Order(7, 10, 100));
        state.repo.contains(new Order(4, 10, 100));
        state.repo.contains(new Order(6, 10, 100));
        state.repo.contains(new Order(5, 10, 100));
        state.repo.contains(new Order(10, 10, 100));
        blackhole.consume(state.repo);
    }

    @Benchmark
    public void ConcurrentHashMapContainsBenchmark(Blackhole blackhole, ConcurrentHashMapFullState state) {
        state.repo.contains(new Order(1, 10, 100));
        state.repo.contains(new Order(9, 10, 100));
        state.repo.contains(new Order(2, 10, 100));
        state.repo.contains(new Order(8, 10, 100));
        state.repo.contains(new Order(3, 10, 100));
        state.repo.contains(new Order(7, 10, 100));
        state.repo.contains(new Order(4, 10, 100));
        state.repo.contains(new Order(6, 10, 100));
        state.repo.contains(new Order(5, 10, 100));
        state.repo.contains(new Order(10, 10, 100));
        blackhole.consume(state.repo);
    }

    @Benchmark
    public void HashSetContainsBenchmark(Blackhole blackhole, HashSetFullState state) {
        state.repo.contains(new Order(1, 10, 100));
        state.repo.contains(new Order(9, 10, 100));
        state.repo.contains(new Order(2, 10, 100));
        state.repo.contains(new Order(8, 10, 100));
        state.repo.contains(new Order(3, 10, 100));
        state.repo.contains(new Order(7, 10, 100));
        state.repo.contains(new Order(4, 10, 100));
        state.repo.contains(new Order(6, 10, 100));
        state.repo.contains(new Order(5, 10, 100));
        state.repo.contains(new Order(10, 10, 100));
        blackhole.consume(state.repo);
    }

    @Benchmark
    public void MutableListContainsBenchmark(Blackhole blackhole, MutableListFullState state) {
        state.repo.contains(new Order(1, 10, 100));
        state.repo.contains(new Order(9, 10, 100));
        state.repo.contains(new Order(2, 10, 100));
        state.repo.contains(new Order(8, 10, 100));
        state.repo.contains(new Order(3, 10, 100));
        state.repo.contains(new Order(7, 10, 100));
        state.repo.contains(new Order(4, 10, 100));
        state.repo.contains(new Order(6, 10, 100));
        state.repo.contains(new Order(5, 10, 100));
        state.repo.contains(new Order(10, 10, 100));
        blackhole.consume(state.repo);
    }

    @Benchmark
    public void THashSetContainsBenchmark(Blackhole blackhole, THashSetFullState state) {
        state.repo.contains(new Order(1, 10, 100));
        state.repo.contains(new Order(9, 10, 100));
        state.repo.contains(new Order(2, 10, 100));
        state.repo.contains(new Order(8, 10, 100));
        state.repo.contains(new Order(3, 10, 100));
        state.repo.contains(new Order(7, 10, 100));
        state.repo.contains(new Order(4, 10, 100));
        state.repo.contains(new Order(6, 10, 100));
        state.repo.contains(new Order(5, 10, 100));
        state.repo.contains(new Order(10, 10, 100));
        blackhole.consume(state.repo);
    }

    @Benchmark
    public void TreeSetContainsBenchmark(Blackhole blackhole, TreeSetFullState state) {
        state.repo.contains(new Order(1, 10, 100));
        state.repo.contains(new Order(9, 10, 100));
        state.repo.contains(new Order(2, 10, 100));
        state.repo.contains(new Order(8, 10, 100));
        state.repo.contains(new Order(3, 10, 100));
        state.repo.contains(new Order(7, 10, 100));
        state.repo.contains(new Order(4, 10, 100));
        state.repo.contains(new Order(6, 10, 100));
        state.repo.contains(new Order(5, 10, 100));
        state.repo.contains(new Order(10, 10, 100));
        blackhole.consume(state.repo);
    }


}
