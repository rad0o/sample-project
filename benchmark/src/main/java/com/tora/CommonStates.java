package com.tora;

import com.tora.entity.Order;
import com.tora.repo.*;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

public class CommonStates {
    @State(Scope.Benchmark)
    public static class OrdersState {
        static Order order1 = new Order(1, 10, 100);
        static Order order2 = new Order(2, 10, 100);
        static Order order3 = new Order(3, 10, 100);
        static Order order4 = new Order(4, 10, 100);
        static Order order5 = new Order(5, 10, 100);
        static Order order6 = new Order(6, 10, 100);
        static Order order7 = new Order(7, 10, 100);
        static Order order8 = new Order(8, 10, 100);
        static Order order9 = new Order(9, 10, 100);
        static Order order10 = new Order(10, 10, 100);
    }

    @State(Scope.Benchmark)
    public static class THashSetState {
        InMemoryRepository<Order> repo = new THashSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class MutableListState {
        InMemoryRepository<Order> repo = new MutableListBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        InMemoryRepository<Order> repo = new ConcurrentHashMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        InMemoryRepository<Order> repo = new TreeSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        InMemoryRepository<Order> repo = new HashSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ArrayListState {
        InMemoryRepository<Order> repo = new ArrayListBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ArrayListFullState {
        InMemoryRepository<Order> repo = new ArrayListBasedRepository<>();

        @Setup(Level.Invocation)
        public void doSetup() {
            repo.add(OrdersState.order1);
            repo.add(OrdersState.order2);
            repo.add(OrdersState.order3);
            repo.add(OrdersState.order4);
            repo.add(OrdersState.order5);
            repo.add(OrdersState.order6);
            repo.add(OrdersState.order7);
            repo.add(OrdersState.order8);
            repo.add(OrdersState.order9);
            repo.add(OrdersState.order10);
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetFullState {
        InMemoryRepository<Order> repo = new HashSetBasedRepository<>();

        @Setup(Level.Invocation)
        public void doSetup() {
            repo.add(OrdersState.order1);
            repo.add(OrdersState.order2);
            repo.add(OrdersState.order3);
            repo.add(OrdersState.order4);
            repo.add(OrdersState.order5);
            repo.add(OrdersState.order6);
            repo.add(OrdersState.order7);
            repo.add(OrdersState.order8);
            repo.add(OrdersState.order9);
            repo.add(OrdersState.order10);
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapFullState {
        InMemoryRepository<Order> repo = new ConcurrentHashMapBasedRepository<>();

        @Setup(Level.Invocation)
        public void doSetup() {
            repo.add(OrdersState.order1);
            repo.add(OrdersState.order2);
            repo.add(OrdersState.order3);
            repo.add(OrdersState.order4);
            repo.add(OrdersState.order5);
            repo.add(OrdersState.order6);
            repo.add(OrdersState.order7);
            repo.add(OrdersState.order8);
            repo.add(OrdersState.order9);
            repo.add(OrdersState.order10);
        }
    }

    @State(Scope.Benchmark)
    public static class MutableListFullState {
        InMemoryRepository<Order> repo = new MutableListBasedRepository<>();

        @Setup(Level.Invocation)
        public void doSetup() {
            repo.add(OrdersState.order1);
            repo.add(OrdersState.order2);
            repo.add(OrdersState.order3);
            repo.add(OrdersState.order4);
            repo.add(OrdersState.order5);
            repo.add(OrdersState.order6);
            repo.add(OrdersState.order7);
            repo.add(OrdersState.order8);
            repo.add(OrdersState.order9);
            repo.add(OrdersState.order10);
        }
    }

    @State(Scope.Benchmark)
    public static class THashSetFullState {
        InMemoryRepository<Order> repo = new THashSetBasedRepository<>();

        @Setup(Level.Invocation)
        public void doSetup() {
            repo.add(OrdersState.order1);
            repo.add(OrdersState.order2);
            repo.add(OrdersState.order3);
            repo.add(OrdersState.order4);
            repo.add(OrdersState.order5);
            repo.add(OrdersState.order6);
            repo.add(OrdersState.order7);
            repo.add(OrdersState.order8);
            repo.add(OrdersState.order9);
            repo.add(OrdersState.order10);
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetFullState {
        InMemoryRepository<Order> repo = new TreeSetBasedRepository<>();

        @Setup(Level.Invocation)
        public void doSetup() {
            repo.add(OrdersState.order1);
            repo.add(OrdersState.order2);
            repo.add(OrdersState.order3);
            repo.add(OrdersState.order4);
            repo.add(OrdersState.order5);
            repo.add(OrdersState.order6);
            repo.add(OrdersState.order7);
            repo.add(OrdersState.order8);
            repo.add(OrdersState.order9);
            repo.add(OrdersState.order10);
        }
    }
}
