package com.tora;

import com.tora.parser.Parser;
import com.tora.user_interface.UserInterface;

public class App {
    public static void main(String[] argv) {
        Parser parser = new Parser();
        UserInterface ui = new UserInterface(parser);
        ui.run();
    }
}