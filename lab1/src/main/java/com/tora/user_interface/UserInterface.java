package com.tora.user_interface;

import com.tora.expression.Expression;
import com.tora.parser.Parser;

import java.util.Scanner;

public class UserInterface {
    private static final Scanner scanner = new Scanner(System.in);
    private final Parser parser;

    public UserInterface(Parser parser) {
        this.parser = parser;
    }

    private void printInfo() {
        System.out.println("Enter an expression or 'exit' to exit: \n");
    }

    private String getInput() {
        System.out.print(">> ");
        return scanner.nextLine().trim().toLowerCase();
    }

    private void displayResult(String expression) {
        try {
            Expression result = this.parser.parse(expression);

            System.out.println("Result: " + result.evaluate());
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }

    public void run() {
        boolean done = false;
        printInfo();

        while(!done) {
            String cmd = getInput();
            if ("exit".equals(cmd)) {
                done = true;
            } else {
                displayResult(cmd);
            }
        }
    }

}
