package com.tora.expression;

public class ValueExpression implements Expression {
    private final Double value;

    public ValueExpression(Double value) {
        this.value = value;
    }

    @Override
    public Double evaluate() throws Exception {
        return value;
    }
}
