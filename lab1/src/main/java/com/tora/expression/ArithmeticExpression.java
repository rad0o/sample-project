package com.tora.expression;

public class ArithmeticExpression implements Expression{
    Expression left, right;
    String operator;

    public ArithmeticExpression(Expression left, Expression right, String operator) {
        this.left = left;
        this.right = right;
        this.operator = operator;
    }

    @Override
    public Double evaluate() throws Exception {
        Double leftEvaluated = left.evaluate();
        Double rightEvaluated = right.evaluate();

        switch (operator) {
            case "+":
                return leftEvaluated + rightEvaluated;
            case "-":
                return leftEvaluated - rightEvaluated;
            case "/":
                return leftEvaluated / rightEvaluated;
            case "*":
                return leftEvaluated * rightEvaluated;
            default:
                throw new Exception("Unknown operator: " + operator);
        }
    }
}
