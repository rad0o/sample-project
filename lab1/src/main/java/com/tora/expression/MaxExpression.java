package com.tora.expression;

public class MaxExpression implements Expression {
    private final Expression left;
    private final Expression right;

    public MaxExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Double evaluate() throws Exception {
        Double lEval = left.evaluate();
        Double rEval = right.evaluate();
        if(lEval > rEval) {
            return lEval;
        }
        return rEval;
    }
}
