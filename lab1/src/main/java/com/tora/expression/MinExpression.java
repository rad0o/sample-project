package com.tora.expression;

public class MinExpression implements Expression{
    private final Expression left;
    private final Expression right;

    public MinExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Double evaluate() throws Exception {
        Double lEval = left.evaluate();
        Double rEval = right.evaluate();
        if(lEval < rEval) {
            return lEval;
        }
        return rEval;
    }
}
