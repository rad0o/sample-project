package com.tora.expression;

public class SquareRootExpression implements Expression {
    private final Expression term;

    public SquareRootExpression(Expression term) {
        this.term = term;
    }

    @Override
    public Double evaluate() throws Exception {
        return Math.sqrt(term.evaluate());
    }
}
