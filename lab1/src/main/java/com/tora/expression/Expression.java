package com.tora.expression;

public interface Expression {
    Double evaluate() throws Exception;
}
