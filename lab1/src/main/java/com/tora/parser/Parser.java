package com.tora.parser;

import com.tora.expression.*;

import java.util.*;
import java.util.stream.Collectors;

enum TokenClass {
    NUMBER,
    FUNCTION,
    OPERATOR,
    L_PARENTHESIS,
    R_PARENTHESIS,
    COMMA
}

public class Parser implements ArithmeticExpressionParser {
    private final List<String> functions = List.of("min", "max", "sqrt");

    private List<String> tokenize(String expressionToTokenize) {
        return Arrays.stream(expressionToTokenize.trim()
                .split(" ")).collect(Collectors.toList());
    }

    private TokenClass classifyToken(String token) throws Exception {
        if (token.matches("^\\d*\\.?\\d*$")) {
            return TokenClass.NUMBER;
        } else if (token.matches("^[-+/*]$")) {
            return TokenClass.OPERATOR;
        } else if (token.equals("(")) {
            return TokenClass.L_PARENTHESIS;
        } else if (token.equals(")")) {
            return TokenClass.R_PARENTHESIS;
        } else if (functions.contains(token)) {
            return TokenClass.FUNCTION;
        } else if (token.equals(",")) {
            return TokenClass.COMMA;
        } else throw new Exception("Unclassifiable token: " + token);
    }

    private Integer getPrecedence(String operator) {
        if (operator.equals("*") || operator.equals("/")) {
            return 2;
        }
        return 1;
    }

    @Override
    public Expression parse(String expressionToParse) throws Exception {
        List<String> tokens = tokenize(expressionToParse);
        Stack<String> opStack = new Stack<>();
        Stack<Expression> output = new Stack<>();

        for (String token : tokens) {
            switch (classifyToken(token)) {
                case NUMBER:
                    output.push(new ValueExpression(Double.parseDouble(token)));
                    break;
                case FUNCTION:
                case L_PARENTHESIS:
                    opStack.push(token);
                    break;
                case OPERATOR:
                    while (
                            !opStack.empty() &&
                            classifyToken(opStack.peek()) == TokenClass.OPERATOR
                            && getPrecedence(opStack.peek()) >= getPrecedence(token)
                    ) {
                        String topOp = opStack.pop();
                        Expression r = output.pop();
                        Expression l = output.pop();
                        output.push(new ArithmeticExpression(l, r, topOp));
                    }

                    opStack.push(token);
                    break;
                case R_PARENTHESIS:
                    while (classifyToken(opStack.peek()) != TokenClass.L_PARENTHESIS) {
                        if (opStack.empty()) {
                            throw new Exception("Unmatched paranthesis");
                        }

                        String operator = opStack.pop();
                        Expression r = output.pop();
                        Expression l = output.pop();
                        output.push(new ArithmeticExpression(l, r, operator));
                    }
                    opStack.pop();

                    if (classifyToken(opStack.peek()) == TokenClass.FUNCTION) {
                        switch (opStack.pop()) {
                            case "max": {
                                Expression r = output.pop();
                                Expression l = output.pop();
                                output.push(new MaxExpression(l, r));
                                break;
                            }
                            case "min": {
                                Expression r = output.pop();
                                Expression l = output.pop();
                                output.push(new MinExpression(l, r));
                                break;
                            }
                            case "sqrt": {
                                output.push(new SquareRootExpression(output.pop()));
                            }
                        }
                    }
            }
        }

        while(!opStack.empty()) {
            if(classifyToken(opStack.peek()) == TokenClass.L_PARENTHESIS) {
                throw new Exception("Mismatched parenthesis");
            }

            String token = opStack.pop();

            if(classifyToken(token) != TokenClass.OPERATOR) {
                throw new Exception("Misplaced token: " + token);
            }

            Expression r = output.pop();
            Expression l = output.pop();
            output.push(new ArithmeticExpression(l, r, token));
        }

        return output.pop();
    }
}
