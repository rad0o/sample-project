package com.tora.parser;


import com.tora.expression.Expression;

// Parses an arithmetic expression in string form and returns it in the internal form
public interface ArithmeticExpressionParser {
    Expression parse(String expressionToParse) throws Exception;
}
