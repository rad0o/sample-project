package org.example.Console;

import org.example.Chat.Chat;
import org.example.ChatClient.ChatClient;
import org.example.Message.Message;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class Console {
    private final ChatClient chatClient;
    private final Scanner scanner = new Scanner(System.in);

    public Console(ChatClient chatClient) throws IOException {
        this.chatClient = chatClient;
        chatClient.start();
    }

    private void printCommands() {
        System.out.println("""
                COMMANDS:\s
                !hello <port> - connect to port\s
                !friends - show connected friends\s
                !byebye - log out\s
                """);
    }

    public void run() throws IOException, ExecutionException, InterruptedException {
        System.out.println("Use !commands for a list of commands\n");
        var done = false;

        while (!done) {
            String cmd = scanner.nextLine().trim();
            List<String> args = Arrays.stream(cmd.split(" ")).toList();

            switch (args.get(0)) {
                case "!commands" -> printCommands();
                case "!hello" -> connectToFriend(Integer.parseInt(args.get(1)));
                case "!byebye" -> done = true;
                case "!friends" -> listFriends();
                case "!chat" -> handleChat(Integer.parseInt(args.get(1)));
                default -> System.out.println("Unknown command");
            }
        }

        this.chatClient.closeAllConnections();
    }

    private void connectToFriend(int port) throws IOException, ExecutionException, InterruptedException {
        System.out.println(chatClient.connectToFriend("127.0.0.1", port));
    }

    private void listFriends() {
        this.chatClient.getChats()
                .forEach((chat) -> System.out.println(chat.getSocket().getPort()));

        if(this.chatClient.getChats().isEmpty()) {
            System.out.println("No friends available");
        }
    }

    private void handleChat(int port) {
        try {
            Chat chat = chatClient.makeChatActive(port);

            boolean done = false;

            var unread = chat.getMessages();
            if (!unread.isEmpty()) {
                System.out.println("Unread messages: ");
                unread.forEach((m) -> System.out.println(port + ": " + m.getMessage()));
            }
            chat.setMessages(new ArrayList<>());
            System.out.println("You are now chatting with " + port);
            while (!done) {
                String message = scanner.nextLine();
                if (Objects.equals(message, "!menu")) {
                    done = true;
                    if(chat.getSocket().isClosed()) {
                        chatClient.removeChat(chat);
                    } else {
                        chatClient.makeChatInnactive();
                    }
                } else if(Objects.equals(message, "!bye") && !chat.getSocket().isClosed()) {
                    done = true;
                    chat.sendMessage(new Message("", Message.MessageType.BYE));
                    chatClient.closeConnection(port);
                }
                else {
                    chat.sendMessage(new Message(message, Message.MessageType.MESSAGE));
                }
            }
        } catch (NoSuchElementException e) {
            System.out.println("No such chat found.");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
