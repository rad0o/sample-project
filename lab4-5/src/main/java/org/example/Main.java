package org.example;

import org.example.ChatClient.ChatClient;
import org.example.Console.Console;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        var chatClient = new ChatClient(Integer.parseInt(args[0]));
        var console = new Console(chatClient);

        console.run();
    }
}