package org.example.Chat;

import org.example.Message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Chat {
    Socket socket;
    ObjectInputStream inputStream;
    ObjectOutputStream outputStream;
    List<Message> messages = new ArrayList<>();
    private final ExecutorService executor = Executors.newFixedThreadPool(1);
    boolean active = false;
    boolean done = false;

    public Chat(Socket socket) throws IOException {
        this.socket = socket;
        this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        this.inputStream = new ObjectInputStream(socket.getInputStream());
    }

    public void sendMessage(Message message) throws IOException {
        if(this.socket.isClosed()) {
            System.out.println("Chat is closed. Type !menu to go back to the menu");
        } else {
            outputStream.writeObject(message);
        }
    }

    public Message receiveMessage() throws IOException, ClassNotFoundException {
        return (Message) inputStream.readObject();
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void listenForMessages() {
        executor.submit(() -> {
            while(!done && !this.socket.isClosed()) {
                try {
                    var message = this.receiveMessage();

                    if(message.getType() == Message.MessageType.BYE) {
                        this.done = true;
                        this.socket.close();
                        System.out.println(this.socket.getPort() + " closed the connection.");
                    } else {
                        if(this.active) {
                            System.out.println(this.socket.getPort() + ": " + message.getMessage());
                        } else this.messages.add(message);
                    }
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Socket getSocket() {
        return socket;
    }

    public void closeChat() throws IOException {
        this.socket.close();
        this.executor.shutdown();
    }
}
