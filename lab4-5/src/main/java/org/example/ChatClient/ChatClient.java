package org.example.ChatClient;

import org.example.Chat.Chat;
import org.example.Message.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatClient {
    private final ExecutorService executor = Executors.newFixedThreadPool(5);
    private final List<Chat> chats = new ArrayList<>();
    private final int clientPort;
    private final ServerSocket ss;

    public ChatClient(int clientPort) throws IOException {
        this.clientPort = clientPort;
        this.ss = new ServerSocket(clientPort);
    }

    public void start() throws IOException {
        executor.submit(() -> {
            while (!this.ss.isClosed()) {
                try {
                    Socket socket = ss.accept();
                    handleNewChat(socket);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private void handleNewChat(Socket socket) throws IOException {
        System.out.println("Received connection request from " + socket.getPort());
        var chat = new Chat(socket);

        synchronized (chats) {
            this.chats.add(chat);
            chat.listenForMessages();
        }

        System.out.println("Sending acknowledgement...");
        chat.sendMessage(new Message("", Message.MessageType.ACK));
    }

    private boolean isConnected(int port) {
        return this.chats.stream().anyMatch((f) -> f.getSocket().getPort() == port);
    }

    public String connectToFriend(String host, int port) throws ExecutionException, InterruptedException {
        try {
            synchronized (chats) {
                if (isConnected(port)) {
                    return "Already connected to " + port;
                }
            }

            System.out.println("Connecting to " + port + "...");
            var socket = new Socket(host, port);

            return executor.submit(() -> {
                try {
                    var chat = new Chat(socket);
                    System.out.println("Waiting for ack...");
                    Message message = chat.receiveMessage();

                    if (message.getType() == Message.MessageType.ACK) {
                        synchronized (chats) {
                            this.chats.add(chat);
                            chat.listenForMessages();
                        }
                        return "Connection successful";
                    } else {
                        return "Connection refused";
                    }
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }).get();
        } catch (Exception e) {
            return "Connection refused";
        }
    }

    public List<Chat> getChats() {
        return chats;
    }

    public Chat makeChatActive(int port) {
        Chat chat = this.chats.stream().filter((c) -> c.getSocket().getPort() == port).findFirst().orElseThrow();

        chat.setActive(true);

        return chat;
    }

    public void makeChatInnactive() {
        this.chats.stream().filter(Chat::isActive).findFirst().ifPresent(currentActive -> currentActive.setActive(false));
    }

    public void closeConnection(int port) throws IOException {
        var chat = this.chats.stream().filter((c) -> c.getSocket().getPort() == port).findFirst().orElseThrow();
        chat.closeChat();
        this.chats.remove(chat);
    }

    public void removeChat(Chat chat) {
        this.chats.remove(chat);
    }

    public void closeAllConnections() throws IOException {
        this.chats.forEach(chat -> {
            try {
                chat.sendMessage(new Message("", Message.MessageType.BYE));
                chat.closeChat();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        this.executor.shutdown();
        this.ss.close();
    }
}
