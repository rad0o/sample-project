package com.tora.repo;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

public class MutableListBasedRepository<T> implements InMemoryRepository<T> {
    MutableList<T> mutableList = Lists.mutable.empty();

    @Override
    public void add(T element) {
        mutableList.add(element);
    }

    @Override
    public boolean contains(T element) {
        return mutableList.contains(element);
    }

    @Override
    public void remove(T element) {
        mutableList.remove(element);
    }
}
