package com.tora.repo;

import gnu.trove.set.hash.THashSet;

public class THashSetBasedRepository<T> implements InMemoryRepository<T> {
    THashSet<T> hashSet = new THashSet<>();

    @Override
    public void add(T element) {
        hashSet.add(element);
    }

    @Override
    public boolean contains(T element) {
        return hashSet.contains(element);
    }

    @Override
    public void remove(T element) {
        hashSet.remove(element);
    }
}
