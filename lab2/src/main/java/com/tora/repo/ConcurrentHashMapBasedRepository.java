package com.tora.repo;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    Map<T, Object> map = new ConcurrentHashMap<>();

    @Override
    public void add(T element) {
        map.put(element, new Object());
    }

    @Override
    public boolean contains(T element) {
        return map.containsKey(element);
    }

    @Override
    public void remove(T element) {
        map.remove(element);
    }
}
